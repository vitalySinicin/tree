var app = angular.module('app',[]);
app.controller('appCtrl', ['$scope', '$rootScope','$http','$q', function ($scope, $rootScope, $http, $q) {

$scope.flatTree = {
    data: [
        { "id": 1, "parentId": 0, "en_text": "Animals", "ru_text": "Животные", "type": 0, children:true },
        { "id": 2, "parentId": 1, "en_text": "Cat", "ru_text": "Кошка", "type": 1,children:true },
        { "id": 3, "parentId": 1, "en_text": "Dog", "ru_text": "Собака", "type": 2,children:true },
        { "id": 4, "parentId": 1, "en_text": "Cow", "ru_text": "Корова", "type": 3 ,children:true},
        { "id": 22, "parentId": 4, "en_text": "Коровка", "ru_text": "Коровка", "type": 33 },
        { "id": 5, "parentId": 2, "en_text": "Abyssinian", "ru_text": "Сиамский", "type": 10 },
        { "id": 6, "parentId": 2, "en_text": "Aegean cat", "ru_text": "Египетский", "type": 11 },
        { "id": 7, "parentId": 2, "en_text": "Australian Mist", "ru_text": "Австралийский", "type": 12 },
        { "id": 8, "parentId": 3, "en_text": "Affenpinscher", "ru_text": "Пинчер", "type": 20 },
        { "id": 9, "parentId": 3, "en_text": "Afghan Hound", "ru_text": "Афганская", "type": 21 },
        { "id": 10, "parentId": 3, "en_text": "Airedale Terrier", "ru_text": "Терьер", "type": 22 },
        { "id": 11, "parentId": 3, "en_text": "Akita Inu", "ru_text": "Акита Иху", "type": 40 },
        { "id": 12, "parentId": 0, "en_text": "Birds", "ru_text": "Птицы", "type": 4,children:true },
        { "id": 13, "parentId": 12, "en_text": "Akekee", "ru_text": "Акака", "type": 41 },
        { "id": 14, "parentId": 12, "en_text": "Arizona Woodpecker", "ru_text": "Аризонский дятел", "type": 42 },
        { "id": 15, "parentId": 12, "en_text": "Black-chinned Sparrow", "ru_text": "Черный воробей", "type": 43,children:true }
    ],
    total: 16
}
$scope.qweTree = {
    data: [
        { "id": 1, "parentId": 0, "en_text": "Animals", "ru_text": "Роботы", "type": 0, children:true },
        { "id": 2, "parentId": 1, "en_text": "Cat", "ru_text": "Кошка", "type": 1 ,children:true},
        { "id": 3, "parentId": 1, "en_text": "Dog", "ru_text": "Собака", "type": 2 },
        { "id": 4, "parentId": 1, "en_text": "Cow", "ru_text": "Корова", "type": 3 },
        { "id": 22, "parentId": 4, "en_text": "Коровка", "ru_text": "Коровка", "type": 33 },
        { "id": 5, "parentId": 2, "en_text": "Abyssinian", "ru_text": "Сиамский", "type": 10 },
        { "id": 6, "parentId": 2, "en_text": "Aegean cat", "ru_text": "Египетский", "type": 11 },
        { "id": 7, "parentId": 2, "en_text": "Australian Mist", "ru_text": "Австралийский", "type": 12 },
        { "id": 8, "parentId": 3, "en_text": "Affenpinscher", "ru_text": "Пинчер", "type": 20 },
        { "id": 9, "parentId": 3, "en_text": "Afghan Hound", "ru_text": "Афганская", "type": 21 },
        { "id": 10, "parentId": 3, "en_text": "Airedale Terrier", "ru_text": "Терьер", "type": 22 },
        { "id": 11, "parentId": 3, "en_text": "Akita Inu", "ru_text": "Акита Иху", "type": 40 },
        { "id": 12, "parentId": 0, "en_text": "Birds", "ru_text": "Утюг", "type": 4 },
        { "id": 13, "parentId": 12, "en_text": "Akekee", "ru_text": "Акака", "type": 41 },
        { "id": 14, "parentId": 12, "en_text": "Arizona Woodpecker", "ru_text": "Аризонский дятел", "type": 42 },
        { "id": 15, "parentId": 0, "en_text": "Black-chinned Sparrow", "ru_text": "3 й узел", "type": 43 }
    ],
    total: 16
}

$scope.id = 'id';
$scope.parentId = 'parentId';
$scope.content = 'ru_text';
$scope.children = 'children';
$scope.closeIcon = 'fa-folder';
$scope.openIcon = 'fa-folder-open';
$scope.currenObj={};


$scope.changeTree = function(){
    console.log('dddd');
    $scope.flatTree = $scope.qweTree;
}

$scope.qwe = function(){
    console.log('qweqweqweqweqw');
}
//Адрес запроса
    $scope.url = '/getTree';
//Загрузка узла
    $scope.restFunc = function (obj) {
        console.log('Передали узел в rest', obj);
        var deferred = $q.defer();
        $http.get($scope.url, {
            params: {
                node:{},
                from: 0,
                to: 3
            }})
                .success(function (data) {
                   deferred.resolve(data);
                   
                })
                .error(function (data, status) {
                    console.log("Ошибка загрузки: код " + status);
                    deferred.reject(status);
                });

                 return deferred.promise;
    };










   //  $scope.draggedListObj = {};
   //  $scope.dropZoneWidth = '615px';
   //  $scope.dropZoneHeight= '65px';
   //  $scope.connectedListDragged = false;
   //  $scope.from = null;
   //  $scope.to = null;
   //  $scope.fromDrop = null;

   //  $scope.settings = {
   //      title: 'Список узлов',
   //      height: '640px',
   //      sortable: true,
   //      display: true,
   //      customIcons: false,
   //      customCheckBox: false,
   //      openFirstLevel:true,
   //      customDecor:true,
   //      treeBgColor :"rgba(97, 97, 97, 0.83)",
   //      closeIcon :'fa-minus',
   //      openIcon :'fa-plus'


   //  };

   //  $scope.outputField="content";
   //  $scope.refreshNodes = false;
   //  $scope.numberOfNodes = 0;
   //  $scope.nodeNameField = 'content';
   //  $scope.hasChildrenField = 'hasChildren';
   //  $scope.currentNode = 'root';
   //  $scope.isLastField = 'isLast';
   //  $scope.nodeObj = {};
   //  $scope.loadInit = {
   //      from: 0,
   //      to: 55
   //  };
   //  $scope.dropInit = {
   //      from: null,
   //      to: null
   //  };

   // $scope.onDrag = function(){

   // };

   //  $scope.onDrop = function(obj){
   //  	console.log('Сработало событие on-drop директивы ng-include-tree --->>', obj);
   //  $scope.from = obj.from.content;
   //  $scope.to = obj.to.content;
   //  };

   //  $scope.dropeZoneOnDrop = function(obj){
   //      console.log('Сработало событие on-drop директивы tree-drop-zone --->> ',obj);
   //      $scope.fromDrop = obj.content;
   //  };

   //  $scope.clearObj = function(){
   //      $scope.nodeObj = {};
   //  };

   //  $scope.controlTree={
   //      clearTree:function(){
   //      }
   //  };

   //  $scope.openIcon = 'fa-minus';
   //  $scope.closeIcon = 'fa-plus';

   //  $scope.restFunc = function () {
   //      console.log('______________________________________________________');
   //      console.log('В restFunc передали:');
   //      console.log('Объект', $scope.nodeObj);
   //      console.log('Загрузить from', $scope.loadInit.from);
   //      console.log('Загрузить to', $scope.loadInit.to);
   //      console.log('Загружаем..');

   //      // $scope.tree = {"data":[
   //      //     {
   //      //         "order": 10,
   //      //         "type": 5,
   //      //         "somecode": "575a7c1462a0cc73aca6fd61",
   //      //         "content": "Ветка1",
   //      //         "hasChildren": false,
   //      //         "container": []
   //      //     }, {
   //      //         "order": 11,
   //      //         "type": 1,
   //      //         "somecode": "575a7c1475842c4314bff78f",
   //      //         "content": "Ветка2",
   //      //         "hasChildren": true,
   //      //         "container": []
   //      //     }, {
   //      //         "order": 12,
   //      //         "type": 4,
   //      //         "somecode": "575a7c14170a78d87449c786",
   //      //         "content": "Ветка3",
   //      //         "hasChildren": true,
   //      //         "container": [ {
   //      //             "order": 10,
   //      //             "type": 5,
   //      //             "somecode": "575a7c1462a0cc73aca6fd61",
   //      //             "content": "Подветка1",
   //      //             "hasChildren": false,
   //      //             "container": []
   //      //         }, {
   //      //             "order": 11,
   //      //             "type": 1,
   //      //             "somecode": "575a7c1475842c4314bff78f",
   //      //             "content": "Подветка2",
   //      //             "hasChildren": true,
   //      //             "container": []
   //      //         }, {
   //      //             "order": 12,
   //      //             "type": 4,
   //      //             "somecode": "575a7c14170a78d87449c786",
   //      //             "content": "Подветка3",
   //      //             "hasChildren": true,
   //      //             "container": []
   //      //         }]
   //      //     }]
   //      // };

   //      // $scope.list ={"data":[
   //      //     {
   //      //         "order": 10,
   //      //         "type": 5,
   //      //         'id':0,
   //      //         "somecode": "575a7c1462a0cc73aca6fd61",
   //      //         "content": "1 элемент",
   //      //         "hasChildren": false,
   //      //         "container": []
   //      //     }, {
   //      //         "order": 11,
   //      //         "type": 1,
   //      //         'id':1,
   //      //         "somecode": "575a7c1475842c4314bff78f",
   //      //         "content": "2 элемент",
   //      //         "hasChildren": false,
   //      //         "container": []
   //      //     }, {
   //      //         "order": 12,
   //      //         "type": 4,
   //      //         'id':2,
   //      //         "somecode": "575a7c14170a78d87449c786",
   //      //         "content": "3 элемент",
   //      //         "hasChildren": false,
   //      //         "container": []
   //      //     }, {
   //      //         "order": 13,
   //      //         "type": 1,
   //      //         'id':3,
   //      //         "somecode": "575a7c141682d0f8915addd1",
   //      //         "content": "4 элемент",
   //      //         "hasChildren": false,
   //      //         "container": []
   //      //     }, {
   //      //         "order": 14,
   //      //         "type": 0,
   //      //         'id':4,
   //      //         "somecode": "575a7c149ea506907787650a",
   //      //         "content": "5 элемент",
   //      //         "hasChildren": false,
   //      //         "container": []
   //      //     }]
   //      // }
   //  };
}]);