//Создание директивы
angular.module('app')
.directive('ngIncludeTree', function($document) {
    return {
        restrict: 'E',
        scope: {
            height: '=',
            sortable: '=', //dnd
            draggable: '=', //dnd drdaggable
            display: '=', //tree visibility
            customIcons: '=',
            customCheckBox: '=',
            refreshNodes: '=', //Обновлять закрытые узлы
            openFirstLevel: '=', //Открывать первый уровень дерева при загрузке
            numberOfNodes: '=', 
            loadedNode: '=',
            nodeNameField: '=', //Поле которое считать именем узла
            hasChildrenField: '=',//Поле которое считать флагом наличия детей
            currentNode: '=',
            openIcon: '=',
            closeIcon: '=',
            restFunc: '&',
            clearObj: '&',
            onDrag: '&',
            onDrop: '&',
            onMove: '&',
            nodeObj: '=',
            loadInit: '=',
            dropInit: '=',
            isLastField: '=',
            controlTree: '=',
            checkedNodesArray: '=',
            draggedListObj:'=',
            connectedListDragged:'='
                //            customDecor:'=',
                //            treeBgColor:'='

        },
        link: function(scope, elem, attrs) {

            //Изменение настроек
            function changeSettings() {
                elem.find('.IsRoot').children('.Content').text(scope.title);
                var treeHeight = scope.height;
                elem.css('height', treeHeight);
                // var sortable = scope.sortable ? 'enable' : 'disable';
                // $(elem.get(0)).sortable(sortable);
                               // if (sortable = 'enable') 
                               //     elem.addClass('dragAndDropOn');
                                  
                               // }else{
                               //      elem.removeClass('dragAndDropOn');
                               // }
                var display = scope.display ? 'block' : 'none';
                elem.css('display', display);
                var customIcons = scope.customIcons;
                if (customIcons) {
                    elem.addClass('customIconsOn');
                } else {
                    elem.removeClass('customIconsOn');
                }
                var customCheckBox = scope.customCheckBox;
                if (customCheckBox) {
                    //Стираем выбор в режиме без чекбоксов
                    elem.find('.checkedNode').each(function(i) {
                        $(this).removeClass('checkedNode');
                    });
                    elem.addClass('customCheckBoxOn');
                } else {
                    elem.removeClass('customCheckBoxOn');
                }
            }

            var readyForLoadingNodes = true;
            var currentNodeObjectId = null;
            var myName = elem.attr('myname');
            // console.log(myName);
            scope.$watch('refreshNodes', function(nv, ov) {
                if (nv !== ov) {
                    changeSettings();
                }

            });
            //Следим за изменением controller scope
            scope.$watchGroup(['height', ' sortable', 'display', 'customIcons', 'customCheckBox'], function(nv, ov) {
                if (nv !== ov) {
                    changeSettings();
                }
            });
            scope.$watch('customDecor', function(nv) {
                if (nv) {
                    // console.log('ddddd');
                }
            });
            //                    console.log(scope.treeBgColor);



            //Ожидаем прихода массива узлов
            scope.$watch('loadedNode', function(nv, ov) {
                // debugger;
                if (nv) {


                    inProgress = false;
                    //Если дерево не просило узлов
                    if (!readyForLoadingNodes) {
                        // console.log(myName + ' мне пришли узлы, я их НЕ беру');
                        return;
                    }

                    if (typeof(nv) === "object" && Object.keys(nv).length !== 0) {
                        // console.log(myName + ' мне пришли узлы, я их беру');
                        AddNodeData(nv.data, nv.total);
                        if (nv.data) {
                            scope.numberOfNodes += nv.data.length;
                        }
                    } else {
                        return;
                    }

                    //                    if (!Array.isArray(nv))
                    //                    {
                    //                        //Если пришел объект
                    //                        var arrayWrap = [];
                    //                        arrayWrap.push(nv);
                    //                        AddNodeData(arrayWrap);
                    //                        scope.numberOfNodes++;
                    //
                    //                    } else {
                    //                        AddNodeData(nv);
                    //                        scope.numberOfNodes += nv.length;
                    //                    }
                    //Если первый уровень не раскрыт до конца
                    if (unloadedListOfNodes.length > 0) {
                        function LoadNextFirstLevelNode() {
                            // console.log(unloadedListOfNodes);
                            var currentNodeId = unloadedListOfNodes.shift();
                            var conf = initConf(currentNodeId);
                            //Загружаем узел
                            var node = elem.find('li#' + currentNodeId).get(0);
                            showLoading(node, true);
                            // Передаем в REST функцию объект узла и настройки from to
                            scope.nodeObj = GetNodeObject(currentNodeId);
                            scope.loadInit.from = conf.left;
                            scope.loadInit.to = conf.interval;
                            currentNode = currentNodeId;
                            //                                scope.$apply();
                            scope.restFunc();
                        }
                        //Загружаем следующий узел первого уровня
                        if (scope.openFirstLevel) {
                            readyForLoadingNodes = true;
                            LoadNextFirstLevelNode();
                        }
                    }

                    // console.log('Получили c сервера', nv);
                }
                 if(scope.draggable){
           //          elem.draggable({
           //              // revert:'invalid',
           //               cursorAt: { left: 5,top:5 },
           //                helper:function(){
           //                             var div = document.createElement('div');
           //                             angular.element(div).css('width','50px');
           //                             angular.element(div).css('height','50px');
           //                             angular.element(div).css('background-color','green');
           //                             cloneObj =   angular.element(div);
           //                              return cloneObj;
           //                           }
           // });
                $('.treeContainer.draggableTree li.Node').draggable({
                            connectToSortable: '.treeContainer.sortableTree',
                             placeholder: 'placeholder_test',
                                    handle: '.Content',
                                     cursorAt: { left: 0,top:20 },
                                    scope:'tree',
                                        helper:function(){
                                        var div = document.createElement('div');
                                        var i = document.createElement('i');
                                        jDiv = angular.element(div);
                                        jI = angular.element(i);
                                        jI.addClass('fa fa-folder');
                                        jI.css('color','#EAB140');
                                        jI.css('font-size','40px');

                                        jDiv.append(jI);

                                        var cloneObj = $(this).clone();
                                        cloneObj.css('display','none');
                                        jDiv.append(cloneObj);
                                        jDiv.addClass('receive-container');

                                        return jDiv;
                                     },
                                     start: function(event, ui){
                                        // debugger;
                                        var currentNodeId = ui.helper.find('.Node').attr('id');
                                        var nodeObj = GetNodeObject(currentNodeId);
                                        scope.$apply(function(){
                                            scope.draggedListObj = nodeObj;
                                        scope.dropInit.from = nodeObj;
                                        // console.log('from',scope.dropInit.from);

                                        });
                                     }

                            
                    });
            }
            });

            function IDGenerator() {
            this.num = 0;

            // this.getId = function() {
            //     var d = new Date().getTime();
            //     var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            //         var r = (d + Math.random() * 16) % 16 | 0;
            //         d = Math.floor(d / 16);
            //         return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
            //     });

            //     return uuid;
            // }
            this.getId = function() {
                return this.num++;
            };
        }


            //Генератор уникальных id
            var IDGen = new IDGenerator();
            //Массив id узлов для открытия первого уровня дерева
            var unloadedListOfNodes = [];
            //Начальные настройки
            //Заголовок корневого узла
            var treeTitle = scope.title;
            //Высота дерева
            var treeHeight = scope.height;
            elem.css('height', treeHeight);
            //Видимость
            var display = scope.display ? 'block' : 'none';
            elem.css('display', display);
            //Пользовательские иконки
            var customIcons = scope.customIcons;
            if (customIcons) {
                elem.addClass('customIconsOn');
            }
            var currentNode = scope.currentNode;
            //Отслеживаем выбраный узел
            scope.$watch('currentNode', function(nv, ov) {
                // currentNode = scope.currentNode;
                // console.log('Новый currentNode', currentNode);
            });
            //Функция разобрать свойства
            var openIcon = scope.openIcon || "fa-folder-open";
            var closeIcon = scope.closeIcon || "fa-folder";
            //Добавляем структуру корневого узла
            elem.append(' <div id = "root" class="treeContainer " left="0"><ul class="Container">' +
                '</ul>' +
                '</li></div>'

            );
              if(scope.sortable){
                        elem.addClass('sortableTree');
                    }
                    if(scope.draggable){
                        elem.addClass('draggableTree');
                    }
                    if(!scope.sortable){
                      elem.addClass('slimTree');  
                    }

            //            elem.find('div.treeContainer').append('<h1>'+Math.floor(Math.random() * (20 - 10 + 1)) +"</h1>");


            //Контейнер содержащий дерево
            var treeContainer = elem.get(0);
            angular.element(treeContainer).find('.Container').get(0);
            var parentId = scope.parentId;
            var structure = scope.structure;
            var primaryId = scope.primaryId;
            var nodeName = scope.nodeName;
            //Загруженная структура дерева
            var data = [];

            //Устанавливаем начальные параметры загрузки
            //                    var confLoad = initConf(elem.find('#root'));
            //                    debugger;
            //                    console.log(confLoad);
            //                    
            //                         scope.loadInit = confLoad;
            //                    
            //                   
            //                     scope.restFunc();


            //Добавляем полученные узлы к уже загруженной структуре
            function AddNodeData(listOfNodes, total) {
                // console.log('readyForLoadingNodes : ' + readyForLoadingNodes);
                //                debugger;

                if (listOfNodes && Array.isArray(listOfNodes)) {
                    if (listOfNodes.length > 0) {
                        for (var i = 0; i < listOfNodes.length; i++) {
                            //Добавляем объект метаданных для узла
                            listOfNodes[i].treeMetaData = {};
                            //присваиваем уникальный id
                            listOfNodes[i].treeMetaData.nodeId = IDGen.getId();
                            listOfNodes[i].treeMetaData.childrenContainer = [];
                            var numberOfLoadedNodes = scope.loadInit.from +
                                scope.loadInit.to;
                            //                            debugger;
                            if (numberOfLoadedNodes >= total) {
                                if (i === listOfNodes.length - 1) {
                                    //                                    debugger;
                                    listOfNodes[i].treeMetaData.isLast = true;
                                }
                            }
                            //                            console.log(numberOfLoadedNodes);
                            //                            console.log("total", total);
                        }
                    }
                    if (currentNode === 'root') {
                        ScrollingFunc();
                        for (var i = 0; i < listOfNodes.length; i++) {
                            data.push(listOfNodes[i]);
                        }

                        AddItems(currentNode, listOfNodes, scope.nodeNameField);
                        //Открываем первый уровень дерева
                        for (var i = 0; i < listOfNodes.length; i++) {
                            if (listOfNodes[i][scope.hasChildrenField]) {
                                unloadedListOfNodes.push(listOfNodes[i].treeMetaData.nodeId);
                            }
                        }
                        //                        console.log('unloadedNodes', unloadedListOfNodes);
                    } else {
                        //                        debugger;
                        function findNode(currentArray) {
                            for (var i = 0; i < currentArray.length; i++) {
                                if (currentArray[i].treeMetaData.nodeId == currentNode) {
                                    for (var j = 0; j < listOfNodes.length; j++) {
                                        currentArray[i].treeMetaData.childrenContainer.push(listOfNodes[j]);
                                    }

                                    return;
                                } else {
                                    if (currentArray[i].treeMetaData.childrenContainer.length > 0) {
                                        findNode(currentArray[i].treeMetaData.childrenContainer);
                                    }
                                }
                            }

                        }
                        findNode(data);
                        AddItems(currentNode, listOfNodes, scope.nodeNameField);
                    }
                }
                readyForLoadingNodes = false;
            }
            //Инициализация параметров загрузки узла
            function initConf(node) {
                //Высота контейнера дерева
                var containerHight = treeContainer.clientHeight;
                //Высота элементов
                var elementsHight = 25;
                //Определяем число элементов для загрузки 
                var numberOfLoadedElements = Math.round(containerHight / elementsHight);
                //Определяем сколько узлов будем загружать
                if (node.left) {
                    var left = node.left;
                } else {
                    var left = 0;
                    numberOfLoadedElements *= 2;
                }
                var loadConf = {
                    //                    action: 'load',
                    left: left,
                    interval: numberOfLoadedElements,
                    //                    id: node.id
                };
                return loadConf;
            }
            //Анимация загрузки узла
            function showLoading(node, on) {
                //                debugger;
                var nodeObj = GetNodeObject(angular.element(node).attr('id'));
                if (nodeObj.usericon) {
                    var objectCloseIcon = nodeObj.usericon.closeicon;
                    var objectOpenIcon = nodeObj.usericon.openicon;
                } else {
                    objectCloseIcon = closeIcon;
                    objectOpenIcon = openIcon;
                }

                var expand = angular.element(node).children('div').get(0);
                if (on) {
                    expand.className = 'ExpandLoading';
                } else {

                    if (angular.element(node).hasClass('ExpandOpen')) {
                        expand.className = 'Expand fa ' + objectOpenIcon;
                    } else {
                        expand.className = 'Expand fa ' + objectCloseIcon;
                    }
                }
            }
            //Открыть/закрыть узел
            function toggleNode(node) {



                if (angular.element(node).attr('id') !== 'root') {
                    var nodeObj = GetNodeObject(angular.element(node).attr('id'));

                    var expand = angular.element(node).children('.Expand').get(0);
                    if (nodeObj.usericon) {
                        var objectCloseIcon = nodeObj.usericon.closeicon;
                        var objectOpenIcon = nodeObj.usericon.openicon;
                    } else {
                        objectCloseIcon = closeIcon;
                        objectOpenIcon = openIcon;
                    }
                    //Если узел property
                    if (angular.element(node).hasClass('isProp')) {
                        objectCloseIcon = "fa-plus-square";
                        objectOpenIcon = "fa-minus-square";
                    }
                    // console.log(nodeObj);
                    if (angular.element(node).hasClass('ExpandOpen')) {
                        expand.className = '';
                        angular.element(expand).addClass('Expand fa ' + objectCloseIcon);
                    } else {
                        expand.className = '';
                        angular.element(expand).addClass('Expand fa ' + objectOpenIcon);
                    }
                    var newClass = angular.element(node).hasClass('ExpandOpen') ? 'ExpandClosed' : 'ExpandOpen';
                    var re = /(^|\s)(ExpandOpen|ExpandClosed)(\s|$)/;
                    node.className = node.className.replace(re, '$1' + newClass + '$3');
                }

            }
            //Перезагружаем узел
            function refreshNode(nodeId) {
                var detectedObject = {};
                //Проверяем текущий массив
                function findNode(currentArray) {

                    for (var i = 0; i < currentArray.length; i++) {
                        if (currentArray[i].treeMetaData.nodeId == nodeId) {
                            detectedObject = currentArray[i];
                            return;
                        } else {
                            if (currentArray[i].treeMetaData.childrenContainer.length > 0) {
                                findNode(currentArray[i].treeMetaData.childrenContainer);
                            }
                        }
                    }

                }
                findNode(data);
                //                detectedObject.treeMetaData.childrenContainer =[];
                currentEl = angular.element('li#' + nodeId);
                currentEl.children('.Container').empty();
                currentEl.attr('left', 0);
                if (currentEl.hasClass('ExpandOpen')) {
                    toggleNode(currentEl[0]);
                }
                currentEl[0].isProcessed = false;
                currentEl[0].left = 0;
            }

            function GetNodeObject(nodeId) {
                // debugger;
                var detectedObject = {};
                //Проверяем текущий массив
                function findNode(currentArray) {

                    for (var i = 0; i < currentArray.length; i++) {
                        if (currentArray[i].treeMetaData.nodeId == nodeId) {
                            detectedObject = currentArray[i];
                            return;
                        } else {
                            if (currentArray[i].treeMetaData.childrenContainer.length > 0) {
                                findNode(currentArray[i].treeMetaData.childrenContainer);
                            }
                        }
                    }

                }
                findNode(data);
                //Клонируем объект и удаляем метаданные дерева
                var cloneObj = Object.assign({}, detectedObject);
                delete cloneObj.treeMetaData;
                return cloneObj;
            }

            function RemoveNode(nodeId) {
                // console.log('Удаляем узел');
                GetNodeObject(nodeId);
            }

            function EditNode(nodeId) {
                // console.log('Редактируем узел');
                var nodeTitle = GetNodeObject(nodeId)[scope.nodeNameField];
                var modal = angular.element('.modal');
                if (modal.hasClass('fade')) {
                    var input = modal.find('.editNodeInput');
                    input.val(nodeTitle);
                    modal.removeClass('fade');
                    modal.addClass('show');
                    input.focus();
                }


            }


            function LoadNode(nodeId, left, interval) {

                //Передаем в REST функцию объект узла и настройки from to
                scope.nodeObj = GetNodeObject(nodeId);
                scope.loadInit.from = left;
                scope.loadInit.to = interval;
                scope.$apply();
                scope.restFunc();
                //                scope.$apply();

            }

            //Добавление узлов в DOM
            function AddItems(node, listOfNodes, nodeNameField) {
                if (node === 'root') {
                    //                    debugger;
                    node = elem.children('.treeContainer').get(0);
                } else {

                    node = elem.find('li#' + node).get(0);
                    showLoading(node, false);
                }
                if (!nodeNameField) {
                    var nodeNameField = 'name';
                }
                if (!node.left) {
                    node.left = 0;
                    $(node).attr('left', 0);
                }

                var elementsNum = 0;
                if (listOfNodes.length !== 0) {
                    node.classList.add('notFull');
                }


                for (var i = 0; i < listOfNodes.length; i++) {
                    var child = listOfNodes[i];
                    //Создаем новый элемент
                    var li = document.createElement('li');
                    //Присваиваем id
                    //                    console.log(child.treeMetaData);
                    li.id = child.treeMetaData.nodeId;
                    //Задаем класс закрытого узла            
                    li.className = "Node ExpandClosed";
                    //  Если загружен последний элемент узла
                    //                    if (listOfNodes[i][scope.isLastField]) {
                    //                        node.fullLoaded = true;
                    //                        node.classList.remove('notFull');
                    //                        li.className += ' IsLast';
                    //                    }
                    if (listOfNodes[i].treeMetaData.isLast) {
                        node.fullLoaded = true;
                        node.classList.remove('notFull');
                        li.className += ' IsLast';
                    }

                    //Добавляем тип узла
                    // li.className += ' type_' + data[i].type;
                    // //Если узел пуст добавляем клас isLeaf
                    if (!listOfNodes[i][scope.hasChildrenField]) {
                        li.className += ' isLeaf';
                        li.isLeaf = true;
                    }
                    //Колличество загруженных узлов 
                    elementsNum = i + 1;
                    if (child.usericon) {
                        var objectCloseIcon = child.usericon.closeicon;
                    } else {
                        var objectCloseIcon = closeIcon;
                    }
                    if (child.isChecked) {
                        var isChecked = 'checked';
                    } else {
                        var isChecked = '';
                    }
                    var editBtnHtml = '<div class="nodeEditButtons">' +
                        '<i class="fa fa-pencil-square-o editBtn" aria-hidden="true"></i>' +
                        '<i class="fa fa-times removeBtn" aria-hidden="true"></i>' +
                        '</div>';
                    //Определяем содержимое узла
                    li.innerHTML = '<input type="checkbox" ' + isChecked + ' class="customCheckBox">' +

                        '<div class="Expand fa ' + objectCloseIcon + '"></div>' +
                        editBtnHtml +
                        '<div class="Content">'
                        //                            + '<input type="checkbox" class="customCheckBox">'
                        + child[nodeNameField] +

                        //                                                            '<div class="nodeEditButtons">' +
                        //                                                            '<i class="fa fa-pencil-square-o editBtn" aria-hidden="true"></i>' +
                        //                                                            '<i class="fa fa-times removeBtn" aria-hidden="true"></i>' +
                        //                                                            '</div>' +
                        '</div>';
                    li.innerHTML += '<ul class="Container"><div class = "empty"></div></ul>';
                    //Добавляем узел в DOM
                    //                    console.log(node.getElementsByTagName('UL')[0]);
                    node.getElementsByTagName('UL')[0].appendChild(li);
                }
                //Устанавливаем кол-во загруженных узлов
                node.left += elementsNum;
                $(node).attr('left', node.left);
                //Устанавливаем признак того что узел уже загружался
                if (!node.isProcessed) {
                    node.isProcessed = true;
                    toggleNode(node);
                }

            }

            //Клик на дереве
            elem.on('mousedown', function(event) {
                //                                debugger;
                // console.log('клик на дереве', elem.find('div.treeContainer'));
                                 // console.log(  elem.find('div.treeContainer'));

                event = event || window.event;
                var clickedElem = angular.element(event.target) ||
                    angular.element(event.srcElement);
                //Определяем какое дерево в фокусе
                //Находим все деревья на странице
                var allDirectives = $document.find('ng-include-tree');
                // console.log(allDirectives);
                //Стираем со всех флаг фокуса
                allDirectives.attr('inFocus', false);
                //Ставим флаг для этого дерева
                elem.attr('inFocus', true);
                //Стираем выбор со всех деревьев
                allDirectives.find('.checkedNode').each(function(i) {
                    $(this).removeClass('checkedNode');
                });
                //Назначаем функцию очистки дерева для текущего дерева
                scope.controlTree.clearTree = function ClearTree() {

                    var treeMainElement = elem.children('div.treeContainer');
                    treeMainElement.remove();
                    elem.append(' <div id = "root" class="treeContainer"><ul class="Container">' +
                        '</ul>' +
                        '</li></div>');
                     if(scope.sortable){
                        elem.addClass('sortableTree');
                    }
                    if(scope.draggable){
                        elem.addClass('draggableTree');
                    }
                    if(!scope.sortable){
                      elem.addClass('slimTree');  
                    }


                    currentNode = 'root';
                    scope.clearObj();
                    //                scope.$apply();
                    data = [];
                    scope.loadInit.from = 0;
                    scope.loadInit.to = 55;
                    readyForLoadingNodes = true;
                    scope.restFunc();
                };

                //Клик по контейнеру дерева
                if (clickedElem.hasClass('treeContainer')) {
                    return;

                }
                //Клик по checkBox
                if (clickedElem.hasClass('customCheckBox')) {

                    if (clickedElem.get(0).checked) {

                        recursiveCheckNode(clickedElem, true);
                        clickedElem.get(0).checked = true;
                    } else {

                        recursiveCheckNode(clickedElem, false);
                        clickedElem.get(0).checked = false;
                    }

                    return;
                }

                //Клик по кнопкам редактирования узла
                if (clickedElem.hasClass('editBtn')) {
                    // console.log('edit');
                    var node = clickedElem.closest('li').get(0);
                    var nodeId = angular.element(node).attr('id');
                    // console.log('Node', nodeId);
                    EditNode(nodeId);
                    return;
                }
                if (clickedElem.hasClass('removeBtn')) {
                    // console.log('remove');
                    var node = clickedElem.closest('li').get(0);
                    var nodeId = angular.element(node).attr('id');
                    // console.log('Node', nodeId);
                    RemoveNode(nodeId);
                    return;
                }
                // // клик не на папке
                if (!clickedElem.hasClass('Expand')) {
                    if (clickedElem.hasClass('customCheckBox')) {
                        if (clickedElem.get(0).checked) {
                            clickedElem.parent().parent().addClass('checkedBox');
                        } else {
                            clickedElem.parent().parent().removeClass('checkedBox');
                        }
                    }
                    //Если включены checkBox'ы ничего не выбираем
                    if (elem.hasClass('customCheckBoxOn')) {
                        //                                return;
                    }
                    elem.find('.checkedNode').each(function(i) {
                        $(this).removeClass('checkedNode');
                    });
                    // console.log(clickedElem);
                    //Клик по контенту узла
                    if (clickedElem.hasClass('Content')) {
                        clickedElem.parent().first().addClass('checkedNode');
                        //?
                        clickedElem.addClass('checkedNode');
                        clickedElem.parent().children('.Expand').addClass('checkedNode');
                        clickedElem.parent().children('.Container').children('.empty').addClass('checkedNode');
                        clickedElem.parent().children('.nodeEditButtons').addClass('checkedNode');
                        var node = clickedElem.parent().get(0);
                        currentNodeObjectId = node.id;
                    }
                    //Клик по пустому элементу контейнера
                    if (clickedElem.hasClass('empty')) {
                        clickedElem.addClass('checkedNode');
                        clickedElem.parent().parent().first().addClass('checkedNode');
                        clickedElem.parent().parent().children('.Expand').addClass('checkedNode');
                        clickedElem.parent().parent().children('.Content').addClass('checkedNode');
                        var node = clickedElem.parent().parent().get(0);
                    }
                    //Клик по контейнеру
                    if (clickedElem.hasClass('Container')) {
                        clickedElem.parent().addClass('checkedNode');
                        clickedElem.children('.empty').addClass('checkedNode');
                        var node = clickedElem.parent().get(0);

                    }


                    //Для вывода в textarea

                    //                    var textarea =angular.element(document.getElementById('comment'));
                    var id = angular.element(node).attr('id');
                    //                         textarea.html('<h2>'+GetNodeObject(id).text+'</h2>');
                    scope.currentNode = GetNodeObject(id);
                    scope.$apply();
                    return;
                }

                // Node, на который кликнули
                var node = clickedElem.parent().get(0);
                //                if (clickedElem.hasClass('treeIcons')) {
                //                    var node = clickedElem.parent().parent().get(0);
                //                    console.log('Да!!!!');
                //                    console.log(node);
                //               }
                var nodeId = clickedElem.parent().attr('id');
                //Вариант с обнулением узла*****************     
                if (angular.element(node).hasClass('ExpandOpen') && !angular.element(node).hasClass('isLeaf') && scope.refreshNodes) {
                    // console.log('Закрываем узел');
                    refreshNode(nodeId);
                    return;
                }
                // debugger;
                if (angular.element(node).hasClass('isLeaf')) {
                    return;

                }
                if (node.isProcessed || node.isLeaf) {
                    // Узел уже обработан (возможно он пуст)
                    toggleNode(node);
                    return;
                }


                // if (angular.element(node).find('li').length) {
                //     // Узел не был обработан, но у него почему-то есть потомки
                //     // Например, эти узлы были в DOM дерева до обработки узла
                //     // Как правило, это "структурные" узлы
                //     // ничего подгружать не надо
                //     toggleNode(node);
                //     return;
                // }
                //  

                currentNode = nodeId;
                var conf = initConf(node);
                //Загружаем узел
                showLoading(node, true);
                readyForLoadingNodes = true;
                // console.log(myName + 'Хочу загрузить с ' + conf.left + ' по ' + conf.interval + ' узлов ');
                LoadNode(nodeId, conf.left, conf.interval);
                // debugger;
                // AddPropItems(nodeId, GetNodeObject(nodeId));
            });

            //Правый клик по узлу
            elem.on('contextmenu', function(event) {
                event = event || window.event;
                event.preventDefault();
                // console.log('из контекста', currentNode);
                nodeId = currentNodeObjectId;
                AddPropItems(nodeId, currentNode);






            });

            function AddPropItems(nodeId, objWithProps) {
                // debugger;
                var nodesList = [];
                for (key in objWithProps) {
                    if (key !== 'container' && key !== 'content' && key !== 'hasChildren') {
                        var obj = {};
                        obj[key] = objWithProps[key];


                        nodesList.push(obj);
                    }
                }

                for (var i = 0; i < nodesList.length; i++) {
                    var child = nodesList[i];
                    child.usericon = {};
                    child.usericon.closeicon = "fa-plus-square";

                    child.usericon.openicon = "fa-minus-square"
                        //Создаем новый элемент
                    var li = document.createElement('li');
                    //Класс закрытого узла 
                    li.className = "Node ExpandClosed";

                    var editBtnHtml = '<div class="nodeEditButtons">' +
                        '<i class="fa fa-pencil-square-o editBtn" aria-hidden="true"></i>' +
                        '<i class="fa fa-times removeBtn" aria-hidden="true"></i>' +
                        '</div>';

                    if (child.usericon) {
                        var objectCloseIcon = child.usericon.closeicon;
                    } else {
                        var objectCloseIcon = closeIcon;
                    }

                    //Узел чекнут
                    if (child.isChecked) {
                        var isChecked = 'checked';
                    } else {
                        var isChecked = '';
                    }

                    var propName;
                    for (key in child) {
                        if (key !== 'usericon') {
                            propName = key;
                        }
                    }

                    //Определяем содержимое узла
                    li.innerHTML = '<input type="checkbox" ' + isChecked + ' class="customCheckBox">' +

                        '<div class="Expand fa ' + objectCloseIcon + '" ></div>' +
                        editBtnHtml +
                        '<div class="Content">'

                    + propName +

                        '</div>';
                    li.innerHTML += '<ul class="Container"><div class = "empty"></div></ul>';



                    var node = elem.find('#' + nodeId);
                    li.isProcessed = true;
                    angular.element(li).addClass('isProp');

                    //*********************Сделали i-oe li
                    // debugger;
                    //Находим все вложенные свойства li
                    FindAllProps(li, child);

                    //Вставляем подготовленный узел в дерево
                    node.children('.Container').prepend(li);


                    //Находим вложенные элементы
                    function FindAllProps(parentLiElem, liObject) {
                        //Находим имя свойства переданного объекта
                        var propName;
                        for (key in liObject) {
                            if (key !== 'usericon') {
                                propName = key;
                            }
                        }
                        var property = liObject[propName];
                        //Проверяем какой тип имеет свойство
                        if (property || property === 0) {
                            if (typeof(property) === "object") {

                                if (Array.isArray(property)) {
                                    if (property.length > 0) {
                                        for (var j = 0; j < property.length; j++) {
                                            var item = property[j];
                                            // debugger;


                                            FindAllProps(li, item);
                                        }
                                    }
                                } else {

                                }
                            } else {

                                pastSubItem(li, property);
                            }

                            //Вставляет в переданый li узел с именем propName
                            function pastSubItem(elem, propName) {
                                var subLi = document.createElement('li');
                                //Класс закрытого узла 
                                subLi.className = "Node ExpandClosed";
                                //Определяем содержимое вложенного узла
                                subLi.innerHTML = '<input type="checkbox"  class="customCheckBox">' +

                                    '<div class="Expand fa fa-minus" ></div>' +
                                    editBtnHtml +
                                    '<div class="Content">'

                                + propName +

                                    '</div>';
                                subLi.innerHTML += '<ul class="Container"><div class = "empty"></div></ul>';
                                angular.element(subLi).addClass('isLeaf');
                                angular.element(subLi).get(0).isProcessed = true;

                                angular.element(elem).children('.Container').append(subLi);

                            }

                        }
                    }

                }

            }




            if (scope.customCheckBox) {
                if (scope.customCheckBox) {
                    //Стираем выбор в режиме без чекбоксов
                    elem.find('.checkedNode').each(function(i) {
                        $(this).removeClass('checkedNode');
                    });
                    elem.addClass('customCheckBoxOn');
                } else {
                    elem.removeClass('customCheckBoxOn');
                }
            }
            //Рекурсивное выделение checkbox'ов
            function recursiveCheckNode(checkBoxElem, checked) {
                // console.log('Функция рекурсивного выбора');


                //Находим чекбоксы вложенных элементов
                var childrenCheckBoxes = angular.element(checkBoxElem).parent().find('.customCheckBox');
                //Находим родительские элементы
                var parentCheckBoxes = angular.element(checkBoxElem).parents('li:not(root)').children('.customCheckBox');
                //Соседние чекбоксы
                var siblings = angular.element(checkBoxElem).parent().parent().parent().children('.Container').children('li').children('.customCheckBox');
                //Если чекбокс был чекнут
                if (checked) {

                    //Убираем check
                    angular.element.each(childrenCheckBoxes, function(i, child) {
                        child.checked = false;
                    });
                    //Снимаем родительские чеки
                    angular.element.each(parentCheckBoxes, function(i, child) {
                        child.checked = false;
                    });


                    //Флаг что хотябы один сосед чекнут
                    var oneChecksFlag = false;
                    angular.element.each(siblings, function(i, child) {

                        if (child.checked) {
                            oneChecksFlag = true;
                        }

                    });
                    //Родитель чека
                    var parentEl = angular.element(checkBoxElem).parent().parent().parent().children('.customCheckBox');
                    //Если ни один сосед не чекнут
                    if (!oneChecksFlag) {
                        if (parentEl) {
                            parentEl.removeClass('childCheck');
                        }
                    } else {
                        parentEl.addClass('childCheck');
                    }

                    //Функция для всех родителей
                    angular.element.each(parentCheckBoxes, function(i, child) {
                        //                        debugger;
                        //Соседи i ого родителя
                        var siblings = angular.element(parentCheckBoxes[i]).parent().parent().parent().children('.Container').children('li').children('.customCheckBox');
                        //Флаг чекнутости соседей
                        var oneChecksFlag = false;
                        angular.element.each(siblings, function(i, child) {

                            if (child.checked || angular.element(child).hasClass('childCheck')) {
                                oneChecksFlag = true;
                            }

                        });
                        // i тый чекбокс
                        var parentEl = angular.element(parentCheckBoxes[i]).parent().parent().parent().children('.customCheckBox');
                        if (!oneChecksFlag) {

                            if (parentEl) {
                                parentEl.removeClass('childCheck');
                            }

                        } else {
                            parentEl.addClass('childCheck');
                        }


                    });

                    NodesChecking();

                } else {
                    //Ставим check
                    //Проставляем чекбоксы уровнями ниже
                    angular.element.each(childrenCheckBoxes, function(i, child) {

                        child.checked = true;
                        angular.element(child).removeClass('childCheck');

                    });
                    //Все ли соседи чекнуты?
                    var allChecksFlag = true;
                    angular.element.each(siblings, function(i, child) {

                        if (!child.checked) {
                            allChecksFlag = false;
                        }

                    });
                    if (allChecksFlag) {

                        var parentEl = angular.element(checkBoxElem).parent().parent().parent().children('.customCheckBox').get(0);
                        if (parentEl) {
                            parentEl.checked = true
                        }
                    } else {
                        var parentEl = angular.element(checkBoxElem).parent().parent().parent().children('.customCheckBox');
                        if (parentEl) {
                            parentEl.addClass('childCheck');
                        }
                    }

                    angular.element.each(parentCheckBoxes, function(i, child) {
                        var siblings = angular.element(parentCheckBoxes[i]).parent().parent().parent().children('.Container').children('li').children('.customCheckBox');

                        var allChecksFlag = true;
                        angular.element.each(siblings, function(i, child) {

                            if (!child.checked) {
                                allChecksFlag = false;
                            }

                        });
                        if (allChecksFlag) {
                            var parentEl = angular.element(parentCheckBoxes[i]).parent().parent().parent().children('.customCheckBox').get(0);
                            if (parentEl) {
                                parentEl.checked = true
                            }
                        } else {
                            var parentEl = angular.element(parentCheckBoxes[i]).parent().parent().parent().children('.customCheckBox');
                            if (parentEl) {
                                parentEl.addClass('childCheck');
                            }
                        }

                    });

                    NodesChecking();

                }

            }


            function NodesChecking() {
                var checkedArray = [];
                if (scope.customCheckBox) {
                    elem.find('.customCheckBox').each(function(i) {
                        if ($(this).get(0).checked) {
                            var curNode = $(this).closest('.Node').get(0);
                            checkedArray.push(curNode.id);
                        }
                    });
                    var outputArray = [];
                    for (var i = 0; i < checkedArray.length; i++) {
                        outputArray.push(GetNodeObject(checkedArray[i], true));
                    }
                    scope.checkedNodesArray = outputArray;
                    scope.$apply();
                    // console.log('Массив выбраных элементов ->', scope.checkedNodesArray);
                }

            }
            //Двойной клик на дереве
            elem.on('dblclick', function(event) {
                event = event || window.event;
                var clickedElem = angular.element(event.target) ||
                    angular.element(event.srcElement);
                //Клик не на папке и включены checkBox'ы
                if (!clickedElem.hasClass('Expand') && elem.hasClass('customCheckBoxOn')) {
                    var currentNode = clickedElem.closest('.Node');
                    var checkBoxElem = currentNode.children('.customCheckBox').get(0);
                    if (!currentNode.hasClass('IsRoot')) {
                        if (!checkBoxElem.checked) {
                            currentNode.addClass('checkedBox');
                            checkBoxElem.checked = true;
                        } else {
                            currentNode.removeClass('checkedBox');
                            checkBoxElem.checked = false;
                        }
                    }
                }
                //                NodesChecking();
            });

            //Перемещение по дереву с помощью клавиш up, down
            //            angular.element(document).keydown(function (event) {
            $document.on('keydown', function(event) {

                //Определяем находиться ли элемент в фокусе
                var focus = elem.attr('inFocus');
                //Если не в фокусе
                if (focus !== 'true') {

                    //                                var allDirectives = $document.find('ng-include-tree');
                    //                                allDirectives.each(function (i) {
                    //                                    var tree = angular.element(this).find('.checkedNode');
                    //                                       tree.each(function (i) {
                    //                                           var elemOnClass =   angular.element(this);
                    //                                        elemOnClass.removeClass('checkedNode');
                    //                                           debugger;
                    //                                        });
                    //                                    });
                    return;
                }

                //Нажали Enter
                // console.log('keyCode:', event.which);
                //Выделяет узел
                function checkNode(node) {
                    node = angular.element(node);
                    node.addClass('checkedNode');
                    node.children('.Expand').addClass('checkedNode');
                    node.children('.Content').addClass('checkedNode');
                    node.children('.Container').children('.empty').addClass('checkedNode');
                    node.children('.nodeEditButtons').addClass('checkedNode');

                    var chekedNodeId = node.attr('id');
                    scope.currentNode = GetNodeObject(chekedNodeId);
                    scope.$apply();


                }

                var treeContainer = elem.find('div.treeContainer');


                //Находим выделенный элемент
                var chekedNode = elem.find('li.checkedNode');
                //Если нашли выделеный узел
                if (chekedNode.get(0)) {
                    //Нажали пробел
                    if (event.which === 32) {
                        //Действие по умолчанию
                        event.preventDefault();
                        //Клик по checkBox

                        var checkBox = chekedNode.children('.customCheckBox');

                        if (checkBox.get(0).checked) {

                            recursiveCheckNode(checkBox, true);
                            checkBox.get(0).checked = false;
                        } else {

                            recursiveCheckNode(checkBox, false);
                            checkBox.get(0).checked = true;
                        }

                        return;


                    }
                    //Нажали Enter
                    if (event.which === 13) {
                        //Действие по умолчанию
                        event.preventDefault();

                        //Id выделенного узла
                        var chekedNodeId = chekedNode.attr('id');
                        //Если узел лист
                        if (angular.element(chekedNode).hasClass('isLeaf')) {
                            return;
                        }

                        //Вариант с обнулением узла*****************     
                        if (angular.element(chekedNode).hasClass('ExpandOpen') && !angular.element(chekedNode).hasClass('isLeaf') && scope.refreshNodes) {
                            // console.log('Закрываем узел');
                            refreshNode(chekedNodeId);
                            return;
                        }
                        if (chekedNode.get(0).isProcessed || chekedNode.get(0).isLeaf) {
                            // Узел уже обработан (возможно он пуст)
                            toggleNode(chekedNode.get(0));
                            return;
                        }


                        if (angular.element(chekedNode).find('li').length) {
                            // Узел не был обработан, но у него почему-то есть потомки
                            // Например, эти узлы были в DOM дерева до обработки узла
                            // Как правило, это "структурные" узлы
                            // ничего подгружать не надо
                            toggleNode(chekedNode.get(0));
                            return;
                        }
                        //                
                        currentNode = chekedNodeId;
                        var conf = initConf(chekedNode.get(0));
                        //Загружаем узел
                        showLoading(chekedNode.get(0), true);
                        LoadNode(chekedNodeId, conf.left, conf.interval);
                        readyForLoadingNodes = true;

                    }
                    if (event.which === 32) {
                        //Действие по умолчанию
                        event.preventDefault();

                    }


                    //Клавиша Up
                    if (event.which === 38) {
                        //Действие по умолчанию
                        event.preventDefault();


                        //Находим соседа с верху

                        var prevLi = chekedNode.prev('li');
                        //Если есть сосед сверху
                        if (prevLi.get(0)) {



                            //Очищаем все элементы от класса выбора
                            elem.find('.checkedNode').each(function(i) {
                                $(this).removeClass('checkedNode');
                            });
                            //Проверяем есть ли вложенная папка и открыт ли узел                            
                            if (prevLi.hasClass('ExpandOpen') && prevLi.children('.Container').children('li').first()) {

                                var childrens = angular.element(prevLi).find('li.ExpandOpen');
                                if (childrens.get(0)) {
                                    var lastPrevLi = childrens.last().children('.Container').children('li').last();
                                    //                                    debugger;
                                } else {
                                    lastPrevLi = prevLi.children('.Container').children('li').last();
                                }

                                checkNode(lastPrevLi);
                                return;
                            }
                            //Выделяем элемент
                            checkNode(prevLi);
                            //                            
                            //Прокручиваем к элементу выше
                            if (prevLi.get(0).offsetTop < treeContainer.scrollTop()) {
                                treeContainer.animate({
                                    scrollTop: prevLi.get(0).offsetTop
                                }, 0);
                            }


                        } else {
                            //Выше нет элемента                            
                            var prevLi = chekedNode.parent().parent('li:not(".treeContainer")');
                            //Очищаем все элементы от класса выбора
                            if (prevLi.get(0)) {
                                elem.find('.checkedNode').each(function(i) {
                                    $(this).removeClass('checkedNode');
                                });
                                //Выделяем элемент
                                checkNode(prevLi);
                                //Если узел выходит за границы - прокручиваем
                                if (prevLi.get(0).offsetTop > treeContainer.scrollTop() + treeContainer.get(0).clientHeight) {
                                    treeContainer.animate({
                                        scrollTop: prevLi.get(0).offsetTop - treeContainer.get(0).clientHeight + prevLi.get(0).clientHeight
                                    }, 0);
                                }
                            }

                            //
                        }

                    }
                    //Клавиша Down

                    if (event.which === 40) {
                        //Действие по умолчанию
                        event.preventDefault();
                        //Сосед снизу выделенного узла
                        var nextLi = chekedNode.next('li');
                        //Если ниже есть элемент
                        //                        if (nextLi.get(0)) {
                        //Очищаем все элементы от выбора
                        //                            elem.find('.checkedNode').each(function (i) {
                        //                                $(this).removeClass('checkedNode');
                        //                            });

                        //Проверяем есть ли вложенная папка и открыт ли узел                            
                        if (chekedNode.hasClass('ExpandOpen') && chekedNode.children('.Container').children('li').first()) {
                            //Первый узел вложенной папки
                            var subNextLi = chekedNode.children('.Container').children('li').first();
                            //Очищаем все элементы от выбора
                            elem.find('.checkedNode').each(function(i) {
                                $(this).removeClass('checkedNode');
                            });
                            //Выделяем узел
                            checkNode(subNextLi);
                            //Если узел выходит за границы - прокручиваем
                            if (subNextLi.get(0).offsetTop > treeContainer.scrollTop() + treeContainer.get(0).clientHeight) {
                                treeContainer.animate({
                                    scrollTop: subNextLi.get(0).offsetTop - treeContainer.get(0).clientHeight + subNextLi.get(0).clientHeight
                                }, 0);
                            }
                        } else {
                            //Закрыт или пустой узел
                            if (nextLi.get(0)) {
                                elem.find('.checkedNode').each(function(i) {
                                    $(this).removeClass('checkedNode');
                                });
                                checkNode(nextLi);
                                //Если узел выходит за границы - прокручиваем
                                if (nextLi.get(0).offsetTop > treeContainer.scrollTop() + treeContainer.get(0).clientHeight) {
                                    treeContainer.animate({
                                        scrollTop: nextLi.get(0).offsetTop - treeContainer.get(0).clientHeight + nextLi.get(0).clientHeight
                                    }, 0);
                                }
                            }


                            if (!nextLi.get(0)) {
                                // console.log('Кончились соседи');
                                //Ниже нет элемента

                                var parents = angular.element(chekedNode).parents('li:not(".treeContainer")');
                                var parentNextLi = [];

                                parents.each(function(i) {

                                    var parent = angular.element(parents[i]);


                                    if (parent.next('li').get(0)) {
                                        parentNextLi.push(parent.next('li').get(0));


                                    }
                                })

                                //                            var parentNextLi = chekedNode.parent().parent().next('li');
                                if (parentNextLi[0]) {
                                    //Очищаем все элементы от выбора
                                    elem.find('.checkedNode').each(function(i) {
                                        $(this).removeClass('checkedNode');
                                    });
                                    //Выделяем узел
                                    checkNode(parentNextLi[0]);
                                    //Если узел выходит за границы - прокручиваем
                                    if (parentNextLi[0].offsetTop > treeContainer.scrollTop() + treeContainer.get(0).clientHeight) {
                                        treeContainer.animate({
                                            scrollTop: parentNextLi[0].offsetTop - treeContainer.get(0).clientHeight + parentNextLi[0].clientHeight
                                        }, 0);
                                    }
                                }
                                //                            //                        
                                //                            //
                            }
                        }

                    }

                } else {
                    //Если не нашли выделеный узел
                    if ((event.which === 38) || (event.which === 40)) {
                        //Находим верхний узел дерева
                        var firstLi = elem.find('.treeContainer').children('.Container').children('li').first();
                        if (firstLi.get(0)) {
                            checkNode(firstLi);
                            //Прокручиваем в начало
                            treeContainer.animate({
                                scrollTop: $(firstLi).offset().top
                            }, 500);

                        }
                    }
                }


            });

            //Функция очистки дерева
            scope.controlTree.clearTree = function ClearTree() {
                // debugger;
                //                debugger;
                var treeMainElement = elem.children('div.treeContainer');
                treeMainElement.remove();
                elem.append(' <div id = "root" class="treeContainer"><ul class="Container">' +
                    '</ul>' +
                    '</li></div>');
               if(scope.sortable){
                        elem.addClass('sortableTree');
                    }
                    if(scope.draggable){
                        elem.addClass('draggableTree');
                    }
                    if(!scope.sortable){
                      elem.addClass('slimTree');  
                    }
                currentNode = 'root';
                scope.clearObj();
                //                scope.$apply();
                data = [];
                scope.loadInit.from = 0;
                scope.loadInit.to = 55;
                readyForLoadingNodes = true;
                scope.restFunc();
            };

            var modal = angular.element('.modal');
            var saveModalBtn = modal.find('.saveModalBtn');
            var abortModalBtn = modal.find('.abortModalBtn');
            abortModalBtn.on('click', function() {
                if (modal.hasClass('show')) {
                    modal.removeClass('show');
                    modal.addClass('fade');
                }
            });
            saveModalBtn.on('click', function() {

            });
            //Флаг выполнения запроса
            var inProgress = false;
            //Прокрутка дерева   
            function ScrollingFunc() {

                elem.find('.treeContainer').on('scroll', function(event) {
                    //                                console.log(myName + 'меня прокручивают');
                    //                                debugger;


                    //                Находим верхний открытый незагруженный до конца узел
                    //                    if (angular.element('.treeContainer.notFull').first().get(0)) {
                    //                        var unloadedNode = angular.element('.treeContainer.notFull').first();
                    //                        //                        console.log(unloadedNode);
                    //                    } else {
                    //
                    //                        var unloadedNode = angular.element('li.ExpandOpen.notFull').first();
                    //                    }
                    if (elem.find('.treeContainer.notFull').first().get(0)) {
                        var unloadedNode = elem.find('div.treeContainer.notFull').first();
                        //                        console.log(unloadedNode);
                    } else {

                        var unloadedNode = elem.find('li.ExpandOpen.notFull').first();
                    }

                    var subUnloadedNodes = unloadedNode.find('li.ExpandOpen.notFull');
                    var subUnloadedNode = subUnloadedNodes.last();

                    if (subUnloadedNode.get(0)) {
                        var parentSubUnloadedNode = angular.element(subUnloadedNode).parent().parent('li');
                        if (parentSubUnloadedNode.get(0)) {
                            var sublings = parentSubUnloadedNode.find('li.ExpandOpen.notFull');
                            var topElem = sublings.first();
                            unloadedNode = topElem;
                        } else {
                            unloadedNode = subUnloadedNodes.first();
                        }
                    }
                    //Если есть узел
                    if (unloadedNode[0]) {
                        //                         debugger;
                        //                        //Находим координаты последнего загруженного дочернего узла 
                        //                        //и вычисляем  значение прокуртки для поздагрузки
                        //                        var container = unloadedNode.find('.Container');
                        //                        var lastChild = container.children().last()[0];
                        //                        var threshold = lastChild.getBoundingClientRect().top +
                        //                            angular.element(treeContainer).scrollTop() - treeContainer.clientHeight - 200;
                        //                        //Подгружаем узел
                        //                        if (angular.element(treeContainer).scrollTop() > threshold && !inProgress) {
                        //                            //                         debugger;
                        //                            console.log('Должны подгружать узлы');
                        //                            //Устанавливаем флаг выполнения запроса
                        //                            inProgress = true;
                        //                            //Получаем id подгружаемого узла
                        //                            var nodeId = unloadedNode[0].id;
                        //                            //Устанавливаем текущий узел для загрузки
                        //                            currentNode = nodeId;
                        //                            var conf = initConf(unloadedNode[0]);
                        //                            //Загружаем узел
                        //                            LoadNode(nodeId, conf.left, conf.interval);
                        //                            //Показываем анимацию звгрузки узла
                        //                            //                        showLoading(unloadedNode[0], true);
                        //
                        //                        }

                        //Пробный вариант
                        //                         Находим координаты последнего загруженного дочернего узла 
                        //и вычисляем  значение прокуртки для поздагрузки

                        //Контейнер подгружаемого узла
                        var container = unloadedNode.first().children('.Container');
                        //Последний узел подгружаемого узла
                        var lastChild = container.children().last().get(0);
                        //Значение top подгружаемого узла
                        var lastChildTop = lastChild.getBoundingClientRect().top;
                        //                                    console.log(lastChildTop);
                        //Прокрутка дерева
                        var treeContainer = elem.find('div.treeContainer');
                        var treeScrollTop = angular.element(treeContainer).scrollTop();
                        //Высота видимого экрана
                        var screenHeight = treeContainer.get(0).clientHeight;
                        //Граница, после пересечения которой происходит подзагрузка
                        var threshold = treeScrollTop + screenHeight;

                        if (lastChildTop + screenHeight < threshold && !inProgress) {

                            //                            console.log('ДОЗАГРУЗКАААА!');
                            //
                            // console.log('Должны подгружать узлы');
                            //Устанавливаем флаг выполнения запроса
                            inProgress = true;
                            //Получаем id подгружаемого узла
                            var nodeId = unloadedNode[0].id;
                            //Устанавливаем текущий узел для загрузки
                            currentNode = nodeId;
                            var conf = initConf(unloadedNode[0]);
                            readyForLoadingNodes = true;
                            // console.log(myName + 'Хочу подгрузить в #' + currentNode + 'узел с ' + conf.left + ' по ' + conf.interval + ' узлов ');
                            //Загружаем узел
                            LoadNode(nodeId, conf.left, conf.interval);


                        }
                    }

                });
            }
            ScrollingFunc();
            //Подсветка уровней
            //            angular.element(treeContainer).on('mousemove', function (event) {
            //                var clickedEl = angular.element(event.target);
            //                angular.element(treeContainer).find('.Node').removeClass('hoverly');
            ////                angular.element(treeContainer).removeClass('hoverly');
            //                if (clickedEl.hasClass('Container') ||
            //                        clickedEl.hasClass('Content') ||
            //                        clickedEl.hasClass('Expand')) {
            //                    clickedEl.parent().addClass('hoverly');
            //                } else if (clickedEl.hasClass('empty')) {
            //                    clickedEl.parent().parent().addClass('hoverly');
            //                }
            //
            //            });



            angular.element(treeContainer).on('contextmenu', function(event) {
                elem.find('.checkedNode').each(function(i) {
                    $(this).removeClass('checkedNode');
                });
            });
            //Подключение jquery-ui sortable
            //            stopPropagation = false;
            // debugger;

      if(scope.sortable){
            $(treeContainer).sortable({
                items: 'li:not(.IsRoot),div.empty', // определяем тип сортируемых элементов
                handle: '.Content', // определяем активную область для Drag-n-drop
                placeholder: 'placeholder_test', //определяем класс указателя
                cancel: '',
                distance: 10,
                connectWith: ".slimTree",
                grid: [5, 5],
                revert: true,
                dropOnEmpty: true,
                'ui-floating': 'auto',
                forcePlaceholderSize: true,
                cursorAt: {
                    left: 5,
                    top: 20
                },
                // scope:'tree',
                //Начало перетаскивания
                start: function(event, ui) {

                    // debugger;
                        if(scope.connectedListDragged){
                            return;
                        }
                        scope.dragStarted = true;
                    //Находим перетаскиваемый элемент
                    var curEl = ui.item;
                    var parentEL = curEl.parent().parent().first();
                    //Если забрали последний элемент
                    // console.log(parentEL.children('.Container').children().length);
                    if (parentEL.children('.Container').children().length == 3) {
                        parentEL.addClass('isLeaf');
                    }
                    //Находим id элемента
                    var nodeId = curEl.attr('id');
                    //Передаем в контроллер объект узла
                    // debugger;
                    scope.dropInit.from = GetNodeObject(nodeId);
                    //Вызываем событие контроллера - перетаскивание
                    scope.onDrag({from:scope.dropInit.from});
                    scope.$apply();
                },
                //Завершение перетаскивания
                stop: function(event, ui) {
                    debugger;
                    ui.item.removeAttr('style');
                    //Опредляем пермещенный элемент
                    if(scope.connectedListDragged){
                        var curEl = scope.dndStoredNode;
                    }else{

                    var curEl = ui.item;
                    }
                    //родительский узел
                    var parentEL = curEl.parent().parent().first();
                    //Если перетаскиваем в пустой узел
                    if (parentEL.hasClass('isLeaf')) {
                        parentEL.removeClass('isLeaf');
                    }
                    //id родительского узла
                    var nodeId = parentEL.attr('id');
                    //Передаем в контроллер объект узла
                    scope.dropInit.to = GetNodeObject(nodeId);
                    // debugger;
                    // console.log('to',scope.dropInit.to);
                    //Вызываем событие контроллера - завершение перетаскивания
                    var containers = $('li');
                    $.each(containers, function(i, child) {
                        $(child).removeClass('placeHover');
                    });

                    // if(scope.connectedListDragged){
                    //     scope.dropInit.to = GetNodeObject(scope.receivedId);
                    // }

                    var dropObj ={to:scope.dropInit.to, from:scope.dropInit.from} 

                    scope.dragStarted = false;
                     scope.connectedListDragged=false;
                    scope.onDrop({obj:dropObj});
                    scope.$apply();
                },

                //Во время перемещения
                sort: function(event, ui) {
                    // debugger;
                        scope.onMove();
                        //Опредляем пермещенный элемент
                        var curEl = ui.item;
                        //родительский узел
                        var parentEL = curEl.parent().parent().first();
                        var containers = $('li');
                        $.each(containers, function(i, child) {
                            $(child).removeClass('placeHover');
                        });
                        $('li.placeholder_test').parent().parent().addClass('placeHover');
                        //                            console.log(document.querySelectorAll('input:hover'));

                    },
                receive:function(event,ui){
// debugger;
                    var receiveContainer =  elem.find('.receive-container');
                    var storedNode = receiveContainer.find('.Node');
                    receiveContainer.before(storedNode);
                    storedNode.css('display','block');
                    receiveContainer.remove();

                    scope.dndStoredNode = storedNode;
                    // scope.dropInit.to = GetNodeObject(nodeId);
                    // scope.onDrop({to:scope.dropInit.to});
                    // scope.$apply();
                },
                activate:function(event, ui){
                    // debugger;
                    if(!scope.dragStarted){

                    scope.connectedListDragged = true;
                     scope.$apply();
                    }
                },
                deactivate:function(event,ui){
                    // debugger;
                }

            });}
            

            // if(scope.draggable){
            //     $('.treeContainer.draggableTree li.Node').draggable({
            //                 connectToSortable: '.treeContainer.sortableTree',
            //                  placeholder: 'placeholder_test',
            //                         handle: '.Content',
            //                          helper: "clone" 
                            
            //         });
            // }





            //Drag and Drop
            // var sortable = scope.sortable ? 'enable' : 'disable';
            // $(elem.get(0)).sortable(sortable);
            // debugger;
              scope.controlTree.clearTree();
        }
    };
});
