angular.module('app')
.directive('treeDropZone', function($document) {
    var linkFunc = function(scope, elem, attrs) {
        var dropZone = elem.find('div.tree-drop-zone');
        dropZone.css('width', scope.width);
        dropZone.css('height', scope.height);
        if (scope.droppable) {
            dropZone.droppable({
            	scope:'tree',
                drop: function(event, ui) {
                    dropZone.removeClass('over');
                    scope.text = '';

                    scope.droppedElementId = ui.draggable.find('.Node').attr('id');
                    scope.droppedElementHtml = ui.draggable.find('.Content').html();
                    scope.text = scope.droppedElementHtml;
                    scope.onDrop({ obj: scope.draggedListObj });
                    scope.connectedListDragged = false;
                    scope.$apply();

                },

                over: function(event, ui) {
                    dropZone.addClass('over');
                },

                out: function(event, ui) {
                    dropZone.removeClass('over');
                }

            });
        }
    }

    return {
        restrict: 'E',
        scope: {
            width: "=", 
            height: '=',
            droppable: "=", // для включения jQuery droppable
            text: "@", // Текст дроп зоны
            draggedListObj: "=", // Объект дерева-списка который начали перетаскивать
            onDrop: '&', //Событие при drop
            connectedListDragged:"="
        },
        template: '<div class="tree-drop-zone"><p>{{text}}</p></div>',
        link: linkFunc
    }

});
