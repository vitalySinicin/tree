angular.module('app')
    .directive('dmTree', function($document, $compile) {
        var linkFunc = function(scope, elem, attrs) {
            var treeContainer = elem.find('#root.treeContainer');
            var innerData; // Внутренняя структура дерева(массив {})
            var innerDataMap = []; //Карта узлов

            //Следим за приходом данных дерева
            scope.$watch('data', function(nv) {
                if (nv && nv.data.length !== 0) {
                    innerData = [];
                    innerDataMap.length = 0;
                    treeContainer.children('ul').empty();
                    // innerData = nv.data;
                    innerDataMap.push({ mapId: 'root', total: nv.total, loadedNodes: 0, processed: true });
                    InnerDataAdd(nv.data);
                    var firstTreeLevel = GetChildren('root');
                    AddItems('root', firstTreeLevel);
                    // debugger;

                }
            });

            //Добавляем загруженные данные во внутреннюю структуру дерева
            function InnerDataAdd(data) {
                for (var i = 0; i < data.length; i++) {
                    // var newId = IDGen.getId();
                    // data[i].mapId = newId;
                    innerData.push(data[i]);
                    //Метаданные для узла
                    // var metaObj = {};
                    // metaObj.mapId = newId;
                    // metaObj.processed = false;
                    // metaObj.loadedNodes = 0;
                    // innerDataMap.push(metaObj);
                }

            }
            //Класс генератора
            function IDGenerator() {

                this.getId = function() {
                    var d = new Date().getTime();
                    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                        var r = (d + Math.random() * 16) % 16 | 0;
                        d = Math.floor(d / 16);
                        return (c == 'x' ? r : (r & 0x3 | 0x8)).toString(16);
                    });

                    return uuid;
                }

            }
            //Создаем генератор
            var IDGen = new IDGenerator();

            // Получение узлов из внутренней структуры по id
            function GetChildren(findId) {
                var children = [];
                for (var i = 0; i < innerData.length; i++) {
                    if (findId === 'root') {
                        if (!innerData[i][scope.parentIdField] || innerData[i][scope.parentIdField] === '0') {
                            children.push(innerData[i]);
                        }
                    } else {
                        if (innerData[i][scope.parentIdField] == findId) {
                            children.push(innerData[i]);
                        }
                    }

                }
                return children;

            }

            function GetNodeObject(nodeId) {
                var detectedObject = {};
                for (var i = 0; i < innerData.length; i++) {
                    if (innerData[i][scope.idField] == nodeId) {
                        detectedObject = innerData[i];
                        return detectedObject;
                    }
                }
                return false;
            }

            //Открываем или закрываем узел
            function toggleNode(nodeId) {
                var node = elem.find('li#' + nodeId);
                var expand = node.children('.Expand');
                //Отрываем или закрываем узел
                if (node.hasClass('ExpandOpen')) {
                    node.removeClass('ExpandOpen');
                    node.addClass('ExpandClosed');
                    expand.removeClass();
                    expand.addClass('Expand fa ' + scope.closeIcon);
                } else {
                    node.removeClass('ExpandClosed');
                    node.addClass('ExpandOpen');
                    expand.removeClass();
                    expand.addClass('Expand fa ' + scope.openIcon);
                }
            }

            function AddItems(nodeId, listOfNodes) {
                // debugger;

                //Находим нужный узел в DOM
                if (nodeId === 'root') {
                    node = treeContainer.get(0);
                } else {

                    node = elem.find('li#' + nodeId).get(0);
                    //     showLoading(node, false);
                    // }
                    // if (!nodeNameField) {
                    //     var nodeNameField = 'name';
                    // }
                    // if (!node.left) {
                    //     node.left = 0;
                    //     $(node).attr('left', 0);
                    // }

                    // var elementsNum = 0;
                    // if (listOfNodes.length !== 0) {
                    //     node.classList.add('notFull');
                }


                for (var i = 0; i < listOfNodes.length; i++) {
                    var child = listOfNodes[i];
                    var li = document.createElement('li');
                    li.id = child.id;

                    //Задаем класс закрытого узла            
                    li.className = "Node ExpandClosed";

                    // if (listOfNodes[i].treeMetaData.isLast) {
                    //     node.fullLoaded = true;
                    //     node.classList.remove('notFull');
                    //     li.className += ' IsLast';
                    // }

                    if (!listOfNodes[i][scope.hasChildrenField]) {
                        li.className += ' isLeaf';
                        li.isLeaf = true;
                    }
                    //Колличество загруженных узлов 
                    elementsNum = i + 1;
                    if (child.usericon) {
                        var objectCloseIcon = child.usericon.closeicon; //Если иконка пришла вместе с данными
                    } else {
                        var objectCloseIcon = scope.closeIcon || 'fa-folder';
                    }
                    if (child.isChecked) {
                        var isChecked = 'checked';
                    } else {
                        var isChecked = '';
                    }
                    var editBtnHtml = '<div class="nodeEditButtons">' +
                        '<i class="fa fa-pencil-square-o editBtn" aria-hidden="true"></i>' +
                        '<i class="fa fa-times removeBtn" aria-hidden="true"></i>' +
                        '</div>';
                    //Определяем содержимое узла
                    li.innerHTML = '<input type="checkbox" ' + isChecked + ' class="customCheckBox">' +

                        '<div class="Expand fa ' + objectCloseIcon + '" ></div>' +
                        editBtnHtml +
                        '<div class="Content">' + child[scope.contentField] +
                        '</div>';
                    li.innerHTML += '<ul class="Container"><div class = "empty"></div></ul>';
                    //Вешаем событие клика по иконке узла
                    angular.element(li).find('.Expand').attr('ng-click', 'expandClick("' + li.id + '")');
                    angular.element(li).find('.Content').attr('ng-click', 'contentClick("' + li.id + '")');
                    //Компилируем узел
                    $compile(li)(scope);

                    //Добавляем узел в DOM
                    node.getElementsByTagName('UL')[0].appendChild(li);
                }
                //Устанавливаем кол-во загруженных узлов
                node.left += elementsNum;
                $(node).attr('left', node.left);
                //Устанавливаем признак того что узел уже загружался
                if (!node.isProcessed) {
                    node.isProcessed = true;
                    // toggleNode(node);
                }

            }

            //Клик по иконке узла
            scope.expandClick = function(nodeId) {
                var node = elem.find('li#' + nodeId);
                //Если лист
                if (node.hasClass('isLeaf')) {
                    return;
                }

                //Если узел не был обработан ( в него не грузили детей)
                if (!node[0].isProcessed) {
                    var children = GetChildren(nodeId);

                    if (children.length) {
                        AddItems(nodeId, children);
                    } else {
                        var obj = GetNodeObject(nodeId);
                        scope.restFunc({ obj: obj }).then(function(data) {
                            console.log('Получили', data);
                            InnerDataAdd(data.data);
                            AddItems(nodeId, data.data);
                            toggleNode(nodeId);
                        });
                        return;
                    }
                }

                toggleNode(nodeId);
            }

            //Клик по контенту узла
            scope.contentClick = function(nodeId) {
                scope.currentObj =  GetNodeObject(nodeId);
            }

            scope.$watch('currentObj', function(nv){
                var nodes = elem.find('li');
                nodes.removeClass('checkedNode');
                var node = angular.element('li#'+ nv.id);
                node.addClass('checkedNode'); 
            });




        }

        return {
            restrict: 'E',
            scope: {
                height: '=', // Высота
                data: '=', // Модель для дерева
                idField: '=', // поле id узлов дерева
                parentIdField: '=', // поле parentId узлов дерева
                contentField: '=', // поле содержание узла
                hasChildrenField: '=', //Поле флаг наличия потомков узла
                restFunc: '&', //Функция получение потомков узла
                currentObj:'=', //Текщий объект дерева(присваивается при клике на узле)

                openIcon: '=', //Иконка открытого узла(fa-icon)
                closeIcon: '=' // Иконка закрытого узла(fa-icon)


            },
            template: ' <div id = "root" class="treeContainer " left="0">' +
                '<ul class="Container">' +
                '</ul>' +
                '</div>',
            link: linkFunc
        }

    });
