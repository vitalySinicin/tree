var http = require('http');
var url = require('url');
var fs = require('fs');
var express = require('express');

var app = express();

app.use(express.static(__dirname + '/public'));
app.get('/getTree', function (req, res) {
    //Файл с деревом
    var fileName = 'data2.json';
    //Получаем из файла дерево json
    var json = fs.readFileSync(fileName, 'utf8');
    if (json != '') {
        var jsonTree = JSON.parse(json);
    } else {
        res.end('[]');
    }
    //Получам строку GET запроса
    var parsedUrl = url.parse(req.url, true).query;
    var node = parsedUrl.node;
    var from = +parsedUrl.from;
    var to = +parsedUrl.to;;
    console.log('Получили', node);
    console.log('Загрузить from', from);
    console.log('Загрузить to', to);
    
    var outPutTree = {};
    var total = 3;
    outPutTree.data = [];
    outPutTree.total = total;

    
    for (var i = from; i < to + from && i< total; i++) {
        // jsonTree.data[i].content += "_" + i;
        outPutTree.data.push(jsonTree.data[i]);

    }

    res.end(JSON.stringify(outPutTree));


});


app.listen(8080, function () {
    console.log('Test tree app listening on port 8080!');
});